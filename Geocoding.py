import pandas as pd
import requests
import urllib

API_key = ''

def get_google_result(address, api_key=None):
    address = urllib.parse.quote_plus(address)
    geocode_url = "https://maps.googleapis.com/maps/api/geocode/json?address={}".format(address)
    if api_key is not None:
        geocode_url = geocode_url + "&key={}".format(api_key)
    results = requests.get(geocode_url)
    json = results.json()
    if 'results' not in json or len(json['results']) == 0:
        return {"formatted_address": "no such address", "latitude": 0.0, "longitude": 0.0}
    answer = json['results'][0]
    output = {
        "formatted_address": answer.get('formatted_address'),
        "latitude": answer.get('geometry').get('location').get('lat'),
        "longitude": answer.get('geometry').get('location').get('lng')}
    return output


def geocode_file(df):
    results = []
    longitude = []
    latitude = []
    addresses = df["Address"].tolist()
    for address in addresses:
        geo_result = get_google_result(address, API_key)
        results.append(geo_result)
    for element in results:
        longitude.append(element['longitude'])
        latitude.append(element['latitude'])
    df['Lat'] = latitude
    df['Long'] = longitude
    return df


